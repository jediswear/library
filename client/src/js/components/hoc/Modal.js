import React from 'react'
import { connect } from 'react-redux'
import {closeModal} from '../../actions/modal.actions'
import PropTypes from 'prop-types'

const Modal = ({ title, submitHandler, children, closeModal }) => {
  return (
    <div className='modal-container'>
      <div className="modal-overlay" onClick={() => closeModal()}></div>
      <div className='modal-content'>
        <div className="modal-header">
          <span className='modal-header__caption'>{title}</span>
          <button
            className='modal-header__btn'
            onClick={() => closeModal()}
          >
            <i className="material-icons">close</i>
          </button>
        </div>
        <div className="modal-body">
          {children}
        </div>
        <div className='modal-footer'>
          {
            submitHandler && <button
              className='modal-footer__btn'
              onClick={() => {
                submitHandler()
                closeModal()
              }}
            >
              Submit
            </button>
          }
          <button
            className='modal-footer__btn'
            onClick={() => closeModal()}
          >
            Close
          </button>
        </div>
      </div>
    </div>
  )
}

const mapDispatchToProps = {
  closeModal
}

Modal.propTypes = {
  title: PropTypes.string,
  submitHandler: PropTypes.func,
  closeModal: PropTypes.func
}

export default connect(() => ({}), mapDispatchToProps)(Modal)
