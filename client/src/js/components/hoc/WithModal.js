import React, {Fragment} from 'react'
import MainModal from '../modals'
import PropTypes from 'prop-types'

const WithModal = ({children}) => {
  return (
    <Fragment>
      <MainModal/>
      {
        children
      }
    </Fragment>
  )
}

WithModal.propTypes = {
  children: PropTypes.element
}

export default WithModal
