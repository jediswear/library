import React, {useState} from 'react'
import PropTypes from 'prop-types'

const InputField = ({ title, type, name, changeHandler, placeholder }) => {
  const [ inputState ] = useState({
    value: undefined
  })

  return (
    <div className='input-container'>
      <span className='input-container__caption'>{title}:</span>
      <input
        className='input-container__field'
        type={type}
        placeholder={placeholder}
        name={name}
        value={inputState.value}
        onChange={e => changeHandler(e)}
      />
    </div>
  )
}

InputField.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  changeHandler: PropTypes.func,
  placeholder: PropTypes.string
}

export default InputField
