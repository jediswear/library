import React from 'react'
import Rating from './Rating'
import PropTypes from 'prop-types'

const BookItem = ({ name, price, author, icon, rating, id, actionHandler }) => {
  return (
    <li className='list-item'>
      <div className='book-info list-item__item-left'>
        <span className='book-info__field book-info__field_title'>{name}</span>
        <span className='book-info__field'>Author: {author}</span>
        <span className='book-info__field'>Price: {price}</span>
        <Rating
          rating={rating}
          id={id}
        />
      </div>
      <button className='list-item__btn' onClick={() => actionHandler()}>
        <i className="material-icons">{icon}</i>
      </button>
    </li>
  )
}

BookItem.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  author: PropTypes.string,
  price: PropTypes.number,
  rating: PropTypes.number,
  icon: PropTypes.string,
  actionHandler: PropTypes.func,
  editCollection: PropTypes.func
}

export default BookItem
