import React, {useState} from 'react'
import {connect} from 'react-redux'
import {editCollection} from '../../actions/collection.actions'
import InputField from '../InputField'
import Modal from '../hoc/Modal'
import PropTypes from 'prop-types'

const EditCollection = ({ editCollection, id }) => {
  const [ collectionState, setState ] = useState({
    name: null,
    description: null
  })

  const changeHandler = e => {
    const elem = e.target
    const value = elem.value

    setState({
      ...collectionState,
      [elem.name]: value
    })
  }

  const clickHandler = () => {
    editCollection(id, collectionState.name, collectionState.description)
  }

  return (
    <Modal
      title='New collection'
      submitHandler={() => clickHandler()}
    >
      <InputField
        title='Name'
        type='text'
        placeholder='collection name'
        name='name'
        changeHandler={e => changeHandler(e)}
      />
      <InputField
        title='Description'
        type='text'
        placeholder='collection description'
        name='description'
        changeHandler={e => changeHandler(e)}
      />
    </Modal>
  )
}

const mapDispatchToProps = {
  editCollection
}

EditCollection.propTypes = {
  editCollection: PropTypes.func,
  id: PropTypes.string
}

export default connect(null, mapDispatchToProps)(EditCollection)
