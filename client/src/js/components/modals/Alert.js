import React from 'react'
import Modal from '../hoc/Modal'
import PropTypes from 'prop-types'

const Alert = ({ children, title }) => {
  return (
    <Modal
      title={title}
    >
      {children}
    </Modal>
  )
}

Alert.propTypes = {
  title: PropTypes.string,
  children: PropTypes.element
}

export default Alert
