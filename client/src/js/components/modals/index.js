import React, {Fragment} from 'react'
import { connect } from 'react-redux'
import CollectionModal from './CollectionModal'
import BookModal from './BookModal'
import AddToCollectionModal from './AddToCollection'
import EditCollection from './EditCollection'
import Alert from './Alert'

const modals = (name, payload) => {
  switch (name) {
    case 'add_collection':
      return <CollectionModal {...payload}/>
    case 'create_book':
      return <BookModal {...payload}/>
    case 'add_book':
      return <AddToCollectionModal {...payload}/>
    case 'edit_collection':
      return <EditCollection {...payload}/>
    case 'smth_goes_wrong':
      return <Alert title='Error message'>{<Fragment>Oooops! Something goes wrong... Try again</Fragment>}</Alert>
    default:
      return null
  }
}

const Modals = ({ activeModal: { name, data } }) => {
  return name && modals(name, data)
}

const mapStateToProps = ({ activeModal }) => ({
  activeModal
})

export default connect(mapStateToProps, null)(Modals)
