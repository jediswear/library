import React, {useState} from 'react'
import {connect} from 'react-redux'
import {createCollection} from '../../actions/collection.actions'
import InputField from '../InputField'
import Modal from '../hoc/Modal'
import PropTypes from 'prop-types'

const CollectionModal = ({createCollection}) => {
  const [ collectionState, setCollection ] = useState({
    name: 'No name',
    description: 'No description'
  })

  const changeHandler = e => {
    const elem = e.target
    const value = elem.value

    setCollection({
      ...collectionState,
      [elem.name]: value
    })
  }

  const clickHandler = () => {
    createCollection(collectionState.name, collectionState.description)
  }

  return (
    <Modal
      title='New collection'
      submitHandler={() => clickHandler()}
    >
      <InputField
        title='Name'
        type='text'
        placeholder='collection name'
        name='name'
        changeHandler={e => changeHandler(e)}
      />
      <InputField
        title='Description'
        type='text'
        placeholder='collection description'
        name='description'
        changeHandler={e => changeHandler(e)}
      />
    </Modal>
  )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = {
  createCollection
}

CollectionModal.propTypes = {
  createCollection: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(CollectionModal)
