import React, { useEffect } from 'react'
import Modal from '../hoc/Modal'
import { addBookToCollection, fetchAllBooks } from '../../actions/book.actions'
import { connect } from 'react-redux'
import BookItem from '../Book-item'
import PropTypes from 'prop-types'

const AddToCollection = ({ fetchAllBooks, book, addBookToCollection, id: collectionId }) => {
  useEffect(() => {
    fetchAllBooks()
  }, [])

  return (
    <Modal
      title='Select book'
    >
      <ul className='modal-books'>
        {
          book.books.map(({ name, price, author, rating, _id }, i) => (
            <BookItem
              key={i}
              name={name}
              price={price}
              rating={rating}
              id={_id}
              index={i}
              icon='add'
              actionHandler={() => addBookToCollection(collectionId, _id)}
            />))
        }
      </ul>
    </Modal>
  )
}

const mapDispatchToProps = {
  fetchAllBooks,
  addBookToCollection
}

const mapStateToProps = ({ book }) => ({
  book
})

AddToCollection.propTypes = {
  book: PropTypes.object,
  addBookToCollection: PropTypes.func,
  id: PropTypes.string,
  fetchAllBooks: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(AddToCollection)
