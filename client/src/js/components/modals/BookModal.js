import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { closeModal, showModal } from '../../actions/modal.actions'
import { createBook } from '../../actions/book.actions'
import { withRouter } from 'react-router'
import InputField from '../InputField'
import Modal from '../hoc/Modal'

const BookModal = ({ createBook, id }) => {
  const [bookState, setBook] = useState({
    name: 'No name',
    author: 'No author',
    price: 0,
    rating: 0
  })

  const changeHandler = e => {
    const elem = e.target
    let value = elem.value

    switch (elem.name) {
      case 'price':
        value = value < 0 ? 0 : value
        elem.value = value
        break
      case 'rating':
        value = value < 1 ? 1 : value
        value = value > 5 ? 5 : value
        elem.value = value
        break
      default:
        showModal('smth_goes_wrong')
        break
    }

    setBook({
      ...bookState,
      [elem.name]: value
    })
  }

  const clickHandler = () => {
    const { name, author, price, rating } = bookState

    createBook(id, name, author, price, rating)
  }

  return (
    <Modal
      title='New book'
      submitHandler={() => clickHandler()}
    >
      <InputField
        title='Name'
        type='text'
        placeholder='Peter Pan e.g.'
        name='name'
        changeHandler={e => changeHandler(e)}
      />
      <InputField
        title='Author'
        type='text'
        placeholder='J. M. Barrie e.g.'
        name='author'
        changeHandler={e => changeHandler(e)}
      />
      <InputField
        title='Price'
        type='number'
        placeholder='400 e.g.'
        name='price'
        changeHandler={e => changeHandler(e)}
      />
      <InputField
        title='Rating'
        type='number'
        placeholder='0-5'
        name='rating'
        changeHandler={e => changeHandler(e)}
      />
    </Modal>
  )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = {
  closeModal,
  createBook
}

BookModal.propTypes = {
  createBook: PropTypes.func,
  id: PropTypes.string
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BookModal))
