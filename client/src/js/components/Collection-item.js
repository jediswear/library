import React from 'react'
import {removeCollection} from '../actions/collection.actions'
import {showModal} from '../actions/modal.actions'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'

const CollectionItem = ({name, id, index, removeCollection, showModal}) => {
  return (
    <li className="list-item">
      <Link
        className='list-item__item-left'
        to={`/collections/${id}`}>
        {index}. {name}
      </Link>
      <button className='list-item__btn' onClick={() => showModal('edit_collection', {id})}>
        <i className="material-icons">edit</i>
      </button>
      <button className='list-item__btn' onClick={() => removeCollection(id)}>
        <i className="material-icons">delete_forever</i>
      </button>
    </li>
  )
}

const mapDispatchToProps = {
  removeCollection,
  showModal
}

const mapStateToProps = ({collections}) => ({
  collections
})

CollectionItem.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string,
  index: PropTypes.number,
  removeCollection: PropTypes.func,
  showModal: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(CollectionItem)
