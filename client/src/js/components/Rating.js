import React, {useState} from 'react'
import {connect} from 'react-redux'
import {rateBook} from '../actions/book.actions'
import PropTypes from 'prop-types'

const Rating = ({ id, rating, rateBook, collections: { currCollection } }) => {
  const [ ratingState, setRating ] = useState(null)

  const stars = calcStars(ratingState || rating)
  const setStars = starsAmount => setRating(starsAmount)

  return (
    <div
      className='book-info__rating'
      onMouseLeave={() => setStars(null)}>
      {
        stars.map((el, i) =>
          <i
            className="material-icons"
            key={i}
            onClick={() => rateBook(id, i + 1, currCollection['_id'])}
            onMouseOver={() => setStars(i + 1)}>{el}</i>)
      }
    </div>
  )
}

const calcStars = (rating) => {
  const stars = new Array(5)
  stars.fill(null)
  return stars.map((el, i) => i < rating ? 'star' : 'star_border')
}

const mapDispatchToProps = {
  rateBook
}

const mapStateToProps = ({ collections }) => ({
  collections
})

Rating.propTypes = {
  id: PropTypes.string,
  rating: PropTypes.number,
  rateBook: PropTypes.func,
  collections: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(Rating)
