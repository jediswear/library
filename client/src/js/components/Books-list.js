import React, {useEffect, Fragment} from 'react'
import {connect} from 'react-redux'
import {fetchAllBooks, removeBook} from '../actions/book.actions'
import {showModal} from '../actions/modal.actions'
import {Link} from 'react-router-dom'
import BookItem from './Book-item'
import PropTypes from 'prop-types'

const BooksList = ({ book, removeBook, match: { params }, showModal, fetchAllBooks }) => {
  useEffect(() => {
    fetchAllBooks()
  }, [])

  return (
    <Fragment>
      <div className="header">
        <Link className='header__caption' to='/collection'>Collections</Link>
        <Link className='header__caption header__caption-active' to='/books'>Books</Link>
        <button
          className='header__btn'
          onClick={() => showModal('create_book', { id: params.id })}
        >new book
        </button>
      </div>
      <ul className='list'>
        {
          book.books.map(({ name, price, author, rating, _id }, i) => (
            <BookItem
              key={i}
              name={name}
              price={price}
              rating={rating}
              author={author}
              id={_id}
              index={i}
              icon='delete_forever'
              actionHandler={() => removeBook(_id)}
            />))
        }
      </ul>
    </Fragment>
  )
}

const mapDispatchToProps = {
  fetchAllBooks,
  removeBook,
  showModal
}

const mapStateToProps = ({ book }) => ({
  book
})

BooksList.propTypes = {
  match: PropTypes.object,
  book: PropTypes.object,
  removeBook: PropTypes.func,
  showModal: PropTypes.func,
  fetchAllBooks: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(BooksList)
