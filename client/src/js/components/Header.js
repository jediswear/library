import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

const Header = ({ render, description }) => {
  const info = description ? <p className='header-info'>{description}</p> : null

  return (
    <Fragment>
      <div className='header'>
        {render()}
      </div>
      {info}
    </Fragment>
  )
}

Header.propTypes = {
  render: PropTypes.func,
  description: PropTypes.string
}

export default Header
