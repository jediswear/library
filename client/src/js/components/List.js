import React from 'react'
import PropTypes from 'prop-types'

const List = ({ render }) => {
  return (
    <ul className='list'>
      {
        render()
      }
    </ul>
  )
}

List.propTypes = {
  render: PropTypes.func
}

export default List
