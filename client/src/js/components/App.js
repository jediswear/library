import React, {Component} from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import CollectionList from './pages/Collection-list'
import Collection from './pages/Collection'
import BooksList from './pages/Books-list'
import WithModal from './hoc/WithModal'

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <WithModal>
          <div className='app'>
            <Switch>
              <Route exact path='/books' component={BooksList}/>
              <Route exact path='/:collections?' component={CollectionList}/>
              <Route exact path='/collections/:id' component={Collection}/>
            </Switch>
          </div>
        </WithModal>
      </BrowserRouter>
    )
  }
}

export default App
