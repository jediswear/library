import React, { useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import { fetchCollections } from '../../actions/collection.actions'
import { showModal } from '../../actions/modal.actions'
import { Link } from 'react-router-dom'
import List from '../List'
import Header from '../Header'
import CollectionItem from '../Collection-item'
import PropTypes from 'prop-types'

const CollectionList = ({ collections, showModal, fetchCollections }) => {
  useEffect(() => {
    fetchCollections()
  }, [])

  return (
    <Fragment>
      <Header
        render={() => (
          <Fragment>
            <Link className='header__caption header__caption-active' to='/collections'>Collections</Link>
            <Link className='header__caption' to='/books'>Books</Link>
            <button className='header__btn' onClick={() => showModal('add_collection')}>
              new
            </button>
          </Fragment>
        )}
      />
      <List render={() => (
        collections.list.map(({ name, _id }, i) =>
          <CollectionItem
            name={name}
            key={i}
            index={i + 1}
            id={_id}
          />
        )
      )}/>
    </Fragment>
  )
}

const mapDispatchToProps = {
  fetchCollections,
  showModal
}

const mapStateToProps = ({ collections }) => ({
  collections
})

CollectionList.propTypes = {
  collections: PropTypes.object,
  showModal: PropTypes.func,
  fetchCollections: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(CollectionList)
