import React, { useEffect, Fragment } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { removeFromCollection } from '../../actions/book.actions'
import { fetchBooks } from '../../actions/collection.actions'
import { showModal } from '../../actions/modal.actions'
import BookItem from '../Book-item'
import Header from '../Header'
import List from '../List'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const Collection = ({ match: { params }, fetchBooks, showModal, removeFromCollection, collections }) => {
  const { currCollection: { books = [], name, description, _id: collectionId } } = collections

  useEffect(() => {
    fetchBooks(params.id)
  }, [])

  return (
    <Fragment>
      <Header
        description={description}
        render={() => (
          <Fragment>
            <Link to='/collections' className='header__back'>
              <i className="material-icons">arrow_back_ios</i>
            </Link>
            <span className='header__caption header__caption-active'>{name}</span>
            <button className='header__btn' onClick={() => showModal('add_book', { id: params.id })}>
              add book
            </button>
          </Fragment>
        )}
      />
      <List render={() => (
        books.map(({ name, price, author, rating, _id: bookId }, i) => (
          <BookItem
            key={i}
            name={name}
            price={price}
            rating={rating}
            id={bookId}
            index={i}
            icon='delete_forever'
            actionHandler={() => removeFromCollection(collectionId, bookId)}
          />))
      )}/>
    </Fragment>
  )
}

const mapDispatchToProps = {
  fetchBooks,
  showModal,
  removeFromCollection
}

const mapStateToProps = ({ collections }) => ({
  collections
})

Collection.propTypes = {
  match: PropTypes.object,
  collections: PropTypes.object,
  fetchBooks: PropTypes.func,
  removeFromCollection: PropTypes.func,
  showModal: PropTypes.func
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Collection))
