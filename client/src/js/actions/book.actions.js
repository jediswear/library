export const FETCH_BOOKS_LIST = 'FETCH_BOOKS_LIST'
export const FETCH_BOOKS_LIST_SUCCEED = 'FETCH_BOOKS_LIST_SUCCEED'
export const CREATE_BOOK = 'CREATE_BOOK'
export const REMOVE_BOOK = 'REMOVE_BOOK'
export const RATE_BOOK = 'RATE_BOOK'
export const ADD_BOOK_TO_COLLECTION = 'ADD_BOOK_TO_COLLECTION'
export const REMOVE_FROM_COLLECTION = 'REMOVE_FROM_COLLECTION'

export const removeFromCollection = (collectionId, bookId) => ({
  type: REMOVE_FROM_COLLECTION,
  collectionId,
  bookId
})

export const createBook = (id, name, author, price, rating = 0) => ({
  type: CREATE_BOOK,
  id,
  name,
  author,
  price,
  rating
})

export const removeBook = (id) => ({
  type: REMOVE_BOOK,
  id
})

export const rateBook = (id, rating, collectionId) => ({
  type: RATE_BOOK,
  id,
  rating,
  collectionId
})

export const fetchAllBooks = () => ({
  type: FETCH_BOOKS_LIST
})

export const addBookToCollection = (collectionId, bookId) => ({
  type: ADD_BOOK_TO_COLLECTION,
  collectionId,
  bookId
})

export const fetchAllBooksSucceed = (books) => ({
  type: FETCH_BOOKS_LIST_SUCCEED,
  books
})
