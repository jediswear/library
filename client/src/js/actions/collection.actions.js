export const FETCH_COLLECTIONS = 'FETCH_COLLECTIONS'
export const FETCH_COLLECTIONS_SUCCEED = 'FETCH_COLLECTIONS_SUCCEED'
export const CREATE_COLLECTION = 'CREATE_COLLECTION'
export const REMOVE_COLLECTION = 'REMOVE_COLLECTION'
export const EDIT_COLLECTION = 'EDIT_COLLECTION'
export const FETCH_COLLECTION_BOOKS = 'FETCH_COLLECTION_BOOKS'
export const FETCH_COLLECTION_BOOKS_SUCCEED = 'FETCH_COLLECTION_BOOKS_SUCCEED'

export const fetchCollections = () => ({
  type: FETCH_COLLECTIONS
})

export const fetchCollectionsSucceed = (collections) => ({
  type: FETCH_COLLECTIONS_SUCCEED,
  payload: collections
})

export const createCollection = (name, description) => ({
  type: CREATE_COLLECTION,
  name,
  description
})

export const editCollection = (id, name, description) => ({
  type: EDIT_COLLECTION,
  id,
  name,
  description
})

export const removeCollection = (id) => ({
  type: REMOVE_COLLECTION,
  id
})

export const fetchBooks = (id) => ({
  type: FETCH_COLLECTION_BOOKS,
  id
})

export const fetchBooksSucceed = (collection) => ({
  type: FETCH_COLLECTION_BOOKS_SUCCEED,
  payload: collection
})
