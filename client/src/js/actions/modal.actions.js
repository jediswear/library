export const SHOW_MODAL = 'SHOW_MODAL'
export const HIDE_MODAL = 'HIDE_MODAL'

export const showModal = (name, data) => ({
  type: SHOW_MODAL,
  name,
  data
})

export const closeModal = () => ({
  type: HIDE_MODAL
})
