import {createStore, combineReducers, applyMiddleware} from 'redux'
import collectionsReducer from './reducers/collectionsReducer'
import createSagaMiddleware from 'redux-saga'
import watchSagas from './sagas'
import booksReducer from './reducers/booksReducer'
import modalReducer from './reducers/modalReducer'

const rootReducer = combineReducers({
  collections: collectionsReducer,
  book: booksReducer,
  activeModal: modalReducer
})

const sagaMiddleware = createSagaMiddleware()

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(watchSagas)

export default store
