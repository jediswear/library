import {HIDE_MODAL, SHOW_MODAL} from '../actions/modal.actions'

const initialState = {
  name: null,
  data: null
}

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        name: action.name,
        data: action.data
      }
    case HIDE_MODAL:
      return {
        name: null,
        data: null
      }
    default:
      return state
  }
}

export default modalReducer
