import {FETCH_BOOKS_LIST_SUCCEED} from '../actions/book.actions'

const initialState = {
  books: []
}

const booksReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_BOOKS_LIST_SUCCEED:
      return {
        ...state,
        books: action.books
      }
    default:
      return state
  }
}

export default booksReducer
