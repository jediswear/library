import {FETCH_COLLECTION_BOOKS_SUCCEED, FETCH_COLLECTIONS_SUCCEED} from '../actions/collection.actions'

const initialState = {
  list: [],
  currCollection: {}
}

const collectionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COLLECTIONS_SUCCEED:
      return {
        ...state,
        list: action.payload
      }
    case FETCH_COLLECTION_BOOKS_SUCCEED:
      return {
        ...state,
        currCollection: action.payload
      }
    default:
      return state
  }
}

export default collectionsReducer
