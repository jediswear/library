import {put} from 'redux-saga/effects'
import {fetchAllBooks, fetchAllBooksSucceed} from '../actions/book.actions'
import {fetchBooks} from '../actions/collection.actions'
import {showModal} from '../actions/modal.actions'

export function * fetchAllBooksSaga () {
  try {
    const res = yield fetch('/api/books')

    if (!res.ok) {
      throw new Error()
    }

    const books = yield res.json()
    yield put(fetchAllBooksSucceed(books))
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * createBookSaga ({id, name, author, price, rating}) {
  const data = {
    name,
    author,
    price,
    rating
  }

  try {
    const res = yield fetch(`/api/books`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    })

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchAllBooks())
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * removeBookSaga ({id}) {
  try {
    const res = yield fetch(`/api/books/${id}`, {method: 'DELETE'})

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchAllBooks())
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * rateBookSaga ({id, rating, collectionId}) {
  const data = {
    rating
  }

  try {
    const res = yield fetch(`/api/books/${id}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    })

    if (!res.ok) {
      throw new Error()
    }

    yield collectionId && put(fetchBooks(collectionId))
    yield put(fetchAllBooks())
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}
