import {put} from 'redux-saga/effects'
import {fetchCollections, fetchCollectionsSucceed, fetchBooks, fetchBooksSucceed} from '../actions/collection.actions'
import {showModal} from '../actions/modal.actions'

export function * fetchCollectionsSaga () {
  try {
    const res = yield fetch('/api/collections')

    if (!res.ok) {
      throw new Error()
    }

    const collections = yield res.json()
    yield put(fetchCollectionsSucceed(collections))
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * fetchCollectionBooksSaga ({id}) {
  try {
    const res = yield fetch(`/api/collections/${id}`)

    if (!res.ok) {
      throw new Error()
    }

    const books = yield res.json()
    yield put(fetchBooksSucceed(books))
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * createCollectionSaga ({name, description}) {
  const data = {
    name,
    description
  }

  try {
    const res = yield fetch(`/api/collections`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    })

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchCollections())
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * removeCollectionSaga ({id}) {
  try {
    const res = yield fetch(`/api/collections/${id}`, {method: 'DELETE'})

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchCollections())
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * addBookToCollectionSaga ({collectionId, bookId}) {
  try {
    const data = {bookId}
    const res = yield fetch(`/api/collections/${collectionId}/books`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    })

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchBooks(collectionId))
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * editCollectionSaga ({id, name, description}) {
  const data = {
    name,
    description
  }

  try {
    const res = yield fetch(`/api/collections/${id}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    })

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchCollections())
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}

export function * removeFromCollectionSaga ({collectionId, bookId}) {
  try {
    const res = yield fetch(`/api/collections/${collectionId}/books/${bookId}`, {method: 'DELETE'})

    if (!res.ok) {
      throw new Error()
    }

    yield put(fetchBooks(collectionId))
  } catch (error) {
    yield put(showModal('smth_goes_wrong'))
  }
}
