import {ADD_BOOK_TO_COLLECTION, CREATE_BOOK, FETCH_BOOKS_LIST, REMOVE_BOOK, RATE_BOOK, REMOVE_FROM_COLLECTION} from '../actions/book.actions'
import {CREATE_COLLECTION, EDIT_COLLECTION, FETCH_COLLECTION_BOOKS, FETCH_COLLECTIONS, REMOVE_COLLECTION} from '../actions/collection.actions'
import {takeEvery} from 'redux-saga/effects'
import {
  addBookToCollectionSaga,
  createCollectionSaga, editCollectionSaga,
  fetchCollectionBooksSaga,
  fetchCollectionsSaga,
  removeCollectionSaga, removeFromCollectionSaga
} from './collections'
import {createBookSaga, fetchAllBooksSaga, rateBookSaga, removeBookSaga} from './books'

export default function * watchSagas () {
  yield takeEvery(FETCH_COLLECTIONS, fetchCollectionsSaga)
  yield takeEvery(FETCH_COLLECTION_BOOKS, fetchCollectionBooksSaga)
  yield takeEvery(CREATE_COLLECTION, createCollectionSaga)
  yield takeEvery(REMOVE_COLLECTION, removeCollectionSaga)
  yield takeEvery(CREATE_BOOK, createBookSaga)
  yield takeEvery(FETCH_BOOKS_LIST, fetchAllBooksSaga)
  yield takeEvery(REMOVE_BOOK, removeBookSaga)
  yield takeEvery(ADD_BOOK_TO_COLLECTION, addBookToCollectionSaga)
  yield takeEvery(RATE_BOOK, rateBookSaga)
  yield takeEvery(EDIT_COLLECTION, editCollectionSaga)
  yield takeEvery(REMOVE_FROM_COLLECTION, removeFromCollectionSaga)
}
