import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import store from './js/store'
import App from './js/components/App'
import './styles/main.scss'

const root = document.getElementById('root')

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  root)
